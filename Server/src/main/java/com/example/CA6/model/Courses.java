package com.example.CA6.model;

import com.example.CA6.service.Course;

import java.util.ArrayList;

public class Courses extends Datas{
    private ArrayList<Crs> allCourses;
    private ArrayList<Crs> finalizedCourses;
    private ArrayList<Crs> nonFinalizedCourses;
    private ArrayList<Crs> waitingCourses;
    private int sumOfUnits;
    private ArrayList<Crs> lastSubmit;
    private int term;

    public Courses() {
        allCourses = new ArrayList<Crs>();
        finalizedCourses = new ArrayList<Crs>();
        nonFinalizedCourses = new ArrayList<Crs>();
        waitingCourses = new ArrayList<Crs>();
        this.lastSubmit = new ArrayList<Crs>();
        sumOfUnits = 0;
        term = 0;
    }

    public ArrayList<Crs> getAllCourses() {
        return allCourses;
    }

    public ArrayList<Crs> getFinalizedCourses() {
        return finalizedCourses;
    }

    public ArrayList<Crs> getNonFinalizedCourses() {
        return nonFinalizedCourses;
    }

    public ArrayList<Crs> getWaitingCourses() {
        return waitingCourses;
    }

    public ArrayList<Crs> getLastSubmit() {
        return lastSubmit;
    }

    public int getSumOfUnits() {
        return sumOfUnits;
    }

    public int getTerm() {
        return term;
    }

    public void setAllCourses(ArrayList<Course> allCourses) {
        for(Course course: allCourses) {
            this.allCourses.add(new Crs(course));
        }
    }

    public void setFinalizedCourses(ArrayList<Course> finalizedCourses) {
        for(Course course: finalizedCourses) {
            this.finalizedCourses.add(new Crs(course));
        }
    }

    public void setNonFinalizedCourses(ArrayList<Course> nonFinalizedCourses) {
        for(Course course: nonFinalizedCourses) {
            this.nonFinalizedCourses.add(new Crs(course));
        }
    }

    public void setWaitingCourses(ArrayList<Course> waitingCourses) {
        for(Course course: waitingCourses) {
            this.waitingCourses.add(new Crs(course));
        }
    }

    public void setLastSubmit(ArrayList<Course> lastSubmit) {
        for(Course course: lastSubmit)
            this.lastSubmit.add(new Crs(course));
    }

    public void setSumOfUnits(int sumOfUnits) {
        this.sumOfUnits = sumOfUnits;
    }

    public void setTerm(int term) {
        this.term = term;
    }
}
