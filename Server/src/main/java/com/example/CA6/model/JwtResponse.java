package com.example.CA6.model;

import java.io.Serializable;

public class JwtResponse{
    private static final long serialVersionUID = -8091879091924046844L;
    private String jwtToken;

    public String getJwtToken() {
        return jwtToken;
    }

    public void setJwtToken(String jwtToken) {
        this.jwtToken = jwtToken;
    }

    public JwtResponse(String jwtToken) {
        this.jwtToken = jwtToken;
    }
}
