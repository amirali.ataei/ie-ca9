package com.example.CA6.model;

import com.example.CA6.service.Course;
import org.json.simple.JSONObject;

import java.util.ArrayList;

import static com.example.CA6.util.Tools.getCourse;

public class Crs {
    private String name;
    private String code;
    private String classCode;
    private int signedUp;
    private int capacity;
    private String instructor;
    private String type;
    private int units;
    private String classTimeStart;
    private String classTimeEnd;
    private ArrayList<String> classTimeDays = new ArrayList<String>();
    private String examTimeStart;
    private String examTimeEnd;
    private ArrayList<String> prerequisitesNamesArray = new ArrayList<String>();

    public Crs(Course course) {
        this.name = course.getName();
        this.code = course.getCode();
        this.classCode = course.getClassCode();
        this.signedUp = course.getSignedUp();
        this.capacity = course.getCapacity();
        this.instructor = course.getInstructor();
        this.type = course.getType();
        this.units = course.getUnits();


        String time = course.getTime();
        this.classTimeStart = time.split("-")[0];
        this.classTimeEnd = time.split("-")[1];

        this.classTimeDays = course.getDays();

        this.examTimeStart = course.getExamStart();
        this.examTimeEnd = course.getExamEnd();

        for(String s: course.getPrerequisites()) {
            try {
                this.prerequisitesNamesArray.add(getCourse(s, "1").getName());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public Crs() {}

    public String getName() {
        return name;
    }

    public String getCode() {
        return code;
    }

    public String getClassCode() {
        return classCode;
    }

    public int getUnits() {
        return units;
    }

    public int getCapacity() {
        return capacity;
    }

    public int getSignedUp() {
        return signedUp;
    }

    public String getInstructor() {
        return instructor;
    }

    public ArrayList<String> getClassTimeDays() {
        return classTimeDays;
    }

    public ArrayList<String> getPrerequisitesNamesArray() {
        return prerequisitesNamesArray;
    }

    public String getClassTimeStart() {
        return classTimeStart;
    }

    public String getClassTimeEnd() {
        return classTimeEnd;
    }

    public String getType() {
        return type;
    }

    public String getExamTimeEnd() {
        return examTimeEnd;
    }

    public String getExamTimeStart() {
        return examTimeStart;
    }
}
