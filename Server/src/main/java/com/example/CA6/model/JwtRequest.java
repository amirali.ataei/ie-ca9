package com.example.CA6.model;

import java.io.Serializable;

public class JwtRequest {
    private static final long serialVersionUID = 5926468583005150707L;
    private String email;
    private String password;

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
