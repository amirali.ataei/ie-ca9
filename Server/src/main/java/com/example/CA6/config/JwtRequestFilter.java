package com.example.CA6.config;

import com.example.CA6.repository.StudentRepository;
import com.example.CA6.service.Student;
import com.example.CA6.util.JwtLink;
import com.example.CA6.util.JwtUtil;
import io.jsonwebtoken.ExpiredJwtException;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.*;
import javax.servlet.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebFilter(filterName = "JwtRequestFilter")
@Component
public class JwtRequestFilter extends OncePerRequestFilter {

    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private JwtLink jwtLink;

    private final StudentRepository studentRepository = StudentRepository.getInstance();

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        final String requestTokenHeader = request.getHeader("Authorization");
        logger.warn(requestTokenHeader);

        String studentId = null;
        String jwtToken = null;
        // JWT Token is in the form "Bearer token". Remove Bearer word and get
        // only the Token
        if (requestTokenHeader != null && requestTokenHeader.startsWith("Bearer ")) {
            jwtToken = requestTokenHeader.substring(7);
            try {
                if(request.getRequestURL().toString().endsWith("password"))
                    studentId = jwtLink.extractId(jwtToken);
                else
                    studentId = jwtUtil.extractId(jwtToken);
            } catch (IllegalArgumentException e) {
                System.out.println("Unable to get JWT Token");
            } catch (ExpiredJwtException e) {
                System.out.println("JWT Token has expired");
            }
        }
        else {
            if(
                    !request.getRequestURL().toString().endsWith("authentication")
                    && !request.getRequestURL().toString().endsWith("students")
                    && !request.getRequestURL().toString().endsWith("link")
            )
            {
                logger.warn(request.getRequestURL().toString());
                response.setStatus(401);
            }
            logger.warn("JWT Token does not begin with Bearer String");
        }
        // Once we get the token validate it.
        if (studentId != null) {
            try {
                Student student = studentRepository.findById(studentId);
                logger.warn(student.getStudentId());
                // if token is valid configure Spring Security to manually set
                // authentication
                if (jwtUtil.validateToken(jwtToken, student)) {
                    request.setAttribute("User", student);
                } else {
                    response.setStatus(403);
                }
//                System.out.println(((Student) request.getAttribute("User")).getStudentId());
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        filterChain.doFilter(request, response);
    }
}
