package com.example.CA6.service;

import com.example.CA6.repository.PrevCourseRepository;
import org.json.simple.JSONObject;

import java.sql.SQLException;
import java.util.ArrayList;

public class Student {
    private String id;
    private String name;
    private String email;
    private String password;
    private String secondName;
    private String birthDate;
    private String field;
    private String faculty;
    private String level;
    private String status;
    private String img;

    private final PrevCourseRepository prevCourseRepository = PrevCourseRepository.getInstance();
//    private String enteredAt;

    public Student(String id,
                   String name,
                   String email,
                   String password,
                   String secondName,
                   String birthDate,
                   String field,
                   String faculty,
                   String level,
                   String status,
                   String img) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
        this.secondName = secondName;
        this.birthDate = birthDate;
        this.field = field;
        this.faculty =  faculty;
        this.level = level;
        this.status = status;
        this.img = img;
        initialTerms();
    }

    private Term[] terms = new Term[12];

    private ArrayList<com.example.CA6.service.Grade> courses = new ArrayList<>();

    private WeeklySchedule weeklySchedule = new WeeklySchedule(this);


    public Student() {
        initialTerms();
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public void initialTerms() {
        for(int i = 1; i < 12; i++) {
            terms[i] = new Term(i);
        }
    }

    public Term[] getTerms() {
        return terms;
    }

    public int getCurrentTerm() {
        for(int i = 1; i < 12; i++) {
            if(terms[i].getGrades().size() == 0)
                return i;
        }
        return 12;
    }

    public String getField() {
        return field;
    }

    public String getFaculty() {
        return faculty;
    }

    public String getLevel() {
        return level;
    }

    public String getStatus() {
        return status;
    }

    public String getImg() {
        return img;
    }

    public boolean hadCourse(String code) {
        for(com.example.CA6.service.Grade course : courses) {
            if(code.equals(course.getCode()))
                return true;
        }
        return false;
    }

    public double getGrade(String code) {
        try {
            return prevCourseRepository.findById(this.id + '-' + code).getGrade();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    public WeeklySchedule getWeeklySchedule() {
        return weeklySchedule;
    }

    public String getStudentId() {
        return id;
    }
    public String getName() { return name; }
    public String getSecondName() { return secondName; }
    public String getBirthDate() { return birthDate; }

    public double getGPA() {
        double GPA = 0.0;
        int units = 0;
        for(com.example.CA6.service.Grade course: courses) {
            double grade = course.getGrade();
            int unit = course.getUnits();
            units += unit;
            GPA += (grade * unit);
        }

        GPA = GPA / units;

        return GPA;

    }

    public int getNumOfPassedUnits() {
        int passedUnits = 0;
        for(com.example.CA6.service.Grade course : courses) {
            double grade = course.getGrade();
            int units = course.getUnits();
            if(grade >= 10)
                passedUnits += units;
        }
        return passedUnits;
    }

    public ArrayList<com.example.CA6.service.Grade> getPassedCourses() {
        ArrayList<com.example.CA6.service.Grade> passedCourses = new ArrayList<>();
        for(com.example.CA6.service.Grade course : courses) {
            double grade = course.getGrade();
            if(grade >= 10)
                passedCourses.add(course);
        }
        return passedCourses;
    }

    public void addCourse(com.example.CA6.service.Grade course) {
        courses.add(course);
        terms[course.getTerm()].addGrade(course);
    }

    public void addToWeeklySchedule(com.example.CA6.service.Course course) throws Exception{
        weeklySchedule.aTWS(course);
    }

    public void removeFromWeeklySchedule(com.example.CA6.service.Course course) {
        this.weeklySchedule.rFWS(course);
    }

    public ArrayList<com.example.CA6.service.Course> getWeeklyScheduleCourses(){
        return weeklySchedule.gWS();
    }

    public void finalizeWeeklySchedule() throws Exception{
        this.weeklySchedule.fWS();
    }

    public void setWeeklySchedule(ArrayList<com.example.CA6.service.Course> courses) {
        weeklySchedule.setWS(courses);
    }

}
