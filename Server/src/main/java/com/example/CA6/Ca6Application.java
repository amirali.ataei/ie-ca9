package com.example.CA6;

import com.example.CA6.repository.*;
import com.example.CA6.service.Course;
import com.example.CA6.service.Grade;
import com.example.CA6.service.Student;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;

import static com.example.CA6.util.Tools.*;

@SpringBootApplication
public class Ca6Application {

	public static void main(String[] args) {
		CourseRepository courseRepository = CourseRepository.getInstance();
		StudentRepository studentRepository = StudentRepository.getInstance();
		CourseGroupRepository courseGroupRepository = CourseGroupRepository.getInstance();
		ClassTimeRepository classTimeRepository = ClassTimeRepository.getInstance();
		ExamTimeRepository examTimeRepository = ExamTimeRepository.getInstance();
		PrevCourseRepository prevCourseRepository = PrevCourseRepository.getInstance();
		CurrCourseRepository currCourseRepository = CurrCourseRepository.getInstance();
		PrerequisitesRepository prerequisitesRepository = PrerequisitesRepository.getInstance();

		try {
			courses = getCourses();
			for(Course course : courses)
				for(String s: course.getPrerequisites()) {
					prerequisitesRepository.insertById(course.getCode(), s);
				}
			students = getStudents();
			for(Student student:students) {
				student.initialTerms();
				ArrayList<Grade> grades = getGrades(student.getStudentId());
				for (Grade course : grades) {
					student.addCourse(course);
				}
			}
			System.out.println("Hello");
		}
		catch (Exception e) {}

		SpringApplication.run(Ca6Application.class, args);
	}

}
