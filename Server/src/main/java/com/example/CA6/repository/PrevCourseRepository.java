package com.example.CA6.repository;

import com.example.CA6.service.Course;
import com.example.CA6.service.Grade;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class PrevCourseRepository extends Repository<Grade, String>{
    private static final String TABLE_NAME = "PrevCourse";
    private static PrevCourseRepository instance;
    private static final String COURSES_JOIN = "Course OUTER JOIN " +
            "PrevCourse " +
            "ON Course.code = PrevCourse.code";

    public static PrevCourseRepository getInstance() {
        if (instance == null) {
            try {
                instance = new PrevCourseRepository();
            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println("error in PrevCourseRepository.create query.");
            }
        }
        return instance;
    }

    private PrevCourseRepository() throws SQLException {
        Connection con = ConnectionPool.getConnection();
        PreparedStatement createTableStatement = con.prepareStatement(
                String.format("CREATE TABLE IF NOT EXISTS %s (\n" +
                        "  `sId` char(225) NOT NULL,\n" +
                        "  `cCode` char(225) NOT NULL,\n" +
                        "  `grade` double DEFAULT NULL,\n" +
                        "  `term` int DEFAULT NULL,\n" +
                        "  PRIMARY KEY (`sId`,`cCode`),\n" +
                        "  KEY `cCode_idx` (`cCode`),\n" +
                        "  CONSTRAINT `cCode1` FOREIGN KEY (`cCode`) REFERENCES `Course` (`code`) ON DELETE CASCADE,\n" +
                        "  CONSTRAINT `sId1` FOREIGN KEY (`sId`) REFERENCES `Student` (`id`) ON DELETE CASCADE\n" +
                        ");", TABLE_NAME)
        );
        createTableStatement.executeUpdate();
        createTableStatement.close();
        con.close();
    }

    @Override
    protected String getFindByIdStatement() {
        return String.format("SELECT " +
                "cCode, " +
                "grade, " +
                "term" +
                " FROM %s WHERE sId = ? AND cCode = ?;", TABLE_NAME);
    }

    @Override
    protected void fillFindByIdValues(PreparedStatement st, String id) throws SQLException {
        st.setString(1, id.split("-")[0]);
        st.setString(2, id.split("-")[1]);
    }

    @Override
    protected String getInsertStatement() {
        return String.format("INSERT INTO %s(sId, cCode, grade, term) Select ?, ?, ?, ? Where not exists(Select * from %s where sId = ? AND cCode = ?);", TABLE_NAME, TABLE_NAME);
    }

    @Override
    protected void fillInsertValues(PreparedStatement st, Grade data) throws SQLException {

    }

    @Override
    protected String getFindAllStatement() {
        return null;
    }

    @Override
    protected void fillFindBySearchTypeValues(PreparedStatement st, String search, String type) throws SQLException {

    }

    @Override
    protected Grade convertResultSetToDomainModel(ResultSet rs) throws SQLException {
        return new Grade(rs.getString(1),
                rs.getDouble(2),
                rs.getInt(3));
    }

    @Override
    protected String getFindBySearchType() {
        return null;
    }

    @Override
    protected ArrayList<Grade> convertResultSetToDomainModelList(ResultSet rs) throws SQLException {
        ArrayList<Grade> grades = new ArrayList<>();
        while (rs.next()) {
            grades.add(this.convertResultSetToDomainModel(rs));
        }
        return grades;
    }

    @Override
    protected void fillInsertByIdValues(PreparedStatement st, Grade obj, String id) throws SQLException {
        st.setString(1, id);
        st.setString(2, obj.getCode());
        st.setDouble(3, obj.getGrade());
        st.setInt(4, obj.getTerm());
        st.setString(5, id);
        st.setString(6, obj.getCode());
    }

    @Override
    protected String getFindBySearch() {
        return null;
    }

    @Override
    protected void fillFindBySearchValues(PreparedStatement st, String search) throws SQLException {

    }

    @Override
    protected void fillFindByTermValues(PreparedStatement st, String id) throws SQLException {

        st.setInt(1, Integer.parseInt(id.split("-")[0]));
        st.setString(2, id.split("-")[1]);
    }

    @Override
    protected String getFindByTermStatement() {
        return String.format("SELECT " +
                "cCode, " +
                "grade, " +
                "term" +
                " FROM %s WHERE term = ? AND sId = ?;", TABLE_NAME);
    }

    @Override
    protected void fillFindByEmailValues(PreparedStatement st, String email) throws SQLException {

    }

    @Override
    protected String getFindByEmailStatement() {
        return null;
    }

}
