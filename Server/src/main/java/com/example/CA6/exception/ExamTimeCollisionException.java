package com.example.CA6.exception;

public class ExamTimeCollisionException extends Exception{
    private String code1;
    private String code2;


    public ExamTimeCollisionException(String code, String _code) {
        this.code1 = code;
        this.code2 = _code;
    }
    public String toString(){
        return "ExamTimeCollisionError" + this.code1 + this.code2;
    }
}
