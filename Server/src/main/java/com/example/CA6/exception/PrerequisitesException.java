package com.example.CA6.exception;

public class PrerequisitesException extends Exception{

    private String name;
    public PrerequisitesException(String _name) {
        name = _name;
    }

    public String toString(){
        return "Doesn't pass some of prerequisites of " + name;
    }
}
