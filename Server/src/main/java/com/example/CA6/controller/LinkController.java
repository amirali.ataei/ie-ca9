package com.example.CA6.controller;

import com.example.CA6.model.EmailRequest;
import com.example.CA6.model.JwtRequest;
import com.example.CA6.model.JwtResponse;
import com.example.CA6.repository.StudentRepository;
import com.example.CA6.service.Student;
import com.example.CA6.util.JwtLink;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;

@RestController
@CrossOrigin
@RequestMapping(value = "link", produces = MediaType.APPLICATION_JSON_VALUE)
public class LinkController {
    @Autowired
    private JwtLink jwtLink;

    StudentRepository studentRepository = StudentRepository.getInstance();

    @PostMapping
    public JwtResponse getLink(@RequestBody EmailRequest emailRequest) {
        String email = emailRequest.getEmail();
        Student student = null;
        try {
            student = studentRepository.findByEmail(email);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        String jwtToken = jwtLink.generateToken(student);
        String link = "http://localhost:3000/forget/" + jwtToken;
        return new JwtResponse(link);
    }
}
