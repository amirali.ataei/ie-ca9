package com.example.CA6.controller;

import com.example.CA6.model.Courses;
import com.example.CA6.repository.CourseRepository;
import com.example.CA6.service.Course;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.ArrayList;
import static com.example.CA6.util.Tools.*;

@RestController
@RequestMapping(value = "/courses", produces = MediaType.APPLICATION_JSON_VALUE)
@CrossOrigin
public class CoursesController {

    private final CourseRepository courseRepository = CourseRepository.getInstance();

    @GetMapping()
    public Courses getAllCourses(
            @RequestParam(value = "search") String search,
            @RequestParam(value = "type") String type) throws SQLException {

        Courses crs = new Courses();
        searchFilter = search;

        if(search.equals(""))
            if(type.equals("all"))
                crs.setAllCourses((ArrayList<Course>) courseRepository.findAll());
            else
                crs.setAllCourses((ArrayList<Course>) courseRepository.findAllByTerm(type));
        else
            if(type.equals("all"))
                crs.setAllCourses((ArrayList<Course>) courseRepository.findBySearch(search));
            else
                crs.setAllCourses((ArrayList<Course>) courseRepository.findBySearchType(search, type));

        crs.setStatusCode(202);
        return crs;
    }
}
