package com.example.CA6.controller;

import com.example.CA6.model.Datas;
import com.example.CA6.model.Std;
import com.example.CA6.repository.StudentRepository;
import com.example.CA6.service.Student;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import static com.example.CA6.util.Tools.getStudent;

@RestController
@CrossOrigin
@RequestMapping(value = "/profiles", produces = MediaType.APPLICATION_JSON_VALUE)
public class ProfilesController {
    private final StudentRepository studentRepository = StudentRepository.getInstance();

    @GetMapping(value = "")
    public Std getGetProfile(@RequestAttribute(name = "User") Student student) {
        Std std;
        try {
            std = new Std(student);
            std.setStatusCode(202);
        } catch (Exception e) {
            std = new Std();
            std.setStatusCode(405);
        }
        return std;
    }
}
