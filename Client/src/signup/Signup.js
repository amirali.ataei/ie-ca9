import { React, Component } from "react";
import "./Login.css";
import { Redirect } from "react-router-dom";
import "react-toastify/dist/ReactToastify.css";
import {toast} from "react-toastify";

class signup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            stdId: "",
            name: "",
            secondName: "",
            email: "",
            password: "",
            // pass_repeat: "",
            birthDate: "",
            faculty: "",
            field: "",
            level: "",
            status: "مشغول به تحصیل",
            img: "http://138.197.181.131:5200/img/default.jpg",
            gpa: 0.0,
            tpu: 0
        };
        document.body.className = "login_page";
    }

    render() {
    const id = this.getId();
    if (id) return <Redirect to={{ pathname: "/" }} />;
    return (
        <form className="login_form" onSubmit={this.handleSubmit}>
            <label>شماره دانشجویی: *</label>
            <input type="text" name="stdId" placeholder="شماره دانشجویی" onChange={this.handleInputChange} required autoFocus/><br/><br/>
          <label>نام: *</label>
          <input type="text" name="name" placeholder="نام" onChange={this.handleInputChange} required autoFocus/><br/><br/>
          <label>نام خانوادگی: *</label>
          <input type="text" name="secondName" placeholder="نام خانوادگی" onChange={this.handleInputChange} required autoFocus/><br/><br/>
          <label>ایمیل: *</label>
          <input type="email" name="email"  placeholder="آدرس ایمیل" onChange={this.handleInputChange} required autoFocus/><br/><br/>
          <label>رمز عبور: *</label>
          <input type="password" name="password" placeholder="رمز عبور" onChange={this.handleInputChange} required autoFocus/><br/><br/>
          {/*<label>تکرار رمز عبور: *</label>*/}
          {/*<input type="password" name="pass_repeat" placeholder="تکرار رمز عبور" onChange={this.handleInputChange} required autoFocus/><br/><br/>*/}
          <label>تاریخ تولد:</label>
          <input type="text" name="birthDate" placeholder="تاریخ تولد" onChange={this.handleInputChange} autoFocus/><br/><br/>
          <label>دانشکده:</label>
          <input type="text" name="faculty" placeholder="دانشکده" onChange={this.handleInputChange} autoFocus/><br/><br/>
          <label>رشته:</label>
          <input type="text" name="field" placeholder="رشته" onChange={this.handleInputChange} autoFocus/><br/><br/>
          <label>مقطع:</label>
          <input type="text" name="level" placeholder="مقطع" onChange={this.handleInputChange} autoFocus/><br/><br/>
          <button type="submit">ثبت نام</button>
            <span className="has-account">
              آیا قبلا ثبت نام کرده اید؟ <a href="/login">ورود</a>
            </span>
          </form>
    );
  }

  handleSubmit = async (event) => {
      event.preventDefault();
      // if (this.state.pass !== this.state.pass_repeat)
      //     toast.error("پسورد را تکرار کنید!");
      // else {
          const apiUrl = `http://87.247.185.122:31304/students`;
          const requestOptions = {
              method: "POST",
              headers: {"Content-Type": "application/json"},
              body: JSON.stringify(this.state),
          };
          const response = await fetch(apiUrl, requestOptions);
          const json = await response.json();
          if (json.id) {
              document.body.className = "";
              this.props.setId(this.state.stdId);
          } else toast.error("شماره دانشجويی نامعتبر!");
      // }
  };

  handleInputChange = (event) => {
      this.setState({ [event.target.name]: event.target.value });
  };

  getId = () => {
    return JSON.parse(localStorage.getItem("jwt"));
  };
}

export default signup;
