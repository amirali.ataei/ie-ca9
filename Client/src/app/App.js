import { React, Component } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import "../common/Reset.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "../common/Fonts/Farsi-Digits/font-face-FD.css";
import "../common/mycollection/font/flaticon.css";
import "../common/All.css";
import Login from "../login/Login";
import ProtectedRoute from "./ProtectedRoute.js";
import Home from "../home/Home";
import Courses from "../courses/Courses";
import Schedule from "../schedule/Schedule";
import Error404 from "../common/Error404";
import Signup from "../signup/Signup";
import Password from "../login/Password";
import SetPassword from "../login/SetPassword";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      jwt: "",
    };
  }

  render() {
    return (
      <Router>
        <Switch>
          <Route path="/login" exact>
            <Login setId={this.setId} />
          </Route>
          <Route path="/signup" exact>
            <Signup setId={this.setId} />
          </Route>
          <Route path="/password" exact>
            <Password setId={this.setId} />
          </Route>
          <ProtectedRoute path="/" exact component={Home} />
          <ProtectedRoute path="/courses" exact component={Courses} />
          <ProtectedRoute path="/schedule" exact component={Schedule} />
          <Route path="/forget">
            <SetPassword setId={this.setId} />
          </Route>
          <Route path="/">
            <Error404 />
          </Route>
        </Switch>
      </Router>
    );
  }

  setId = (jwtToken) => {
    localStorage.setItem("jwt", jwtToken);
    this.setState({ jwt: jwtToken });
  };
}

export default App;
