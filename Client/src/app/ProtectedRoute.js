import { React, Component } from "react";
import { Redirect } from "react-router-dom";

class ProtectedRoute extends Component {
  render() {
    const Component = this.props.component;
    const id = this.getId();

    return id ? <Component /> : <Redirect to={{ pathname: "/login" }} />;
  }
  getId = () => {
    return (localStorage.getItem("jwt"));
  };
}

export default ProtectedRoute;
