import { React, Component } from "react";
import "./Login.css";
import logo from "../common/photos/logo.png";
import { Redirect } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";


class SetPassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            password: "",
            password_repeat: "",
        };
        document.body.className = 'login_page';
    }

    render() {
        const id = this.getId();
        if (id) return <Redirect to={{ pathname: "/" }} />;
        return (
            <>
                <ToastContainer
                    position="top-center"
                    autoClose={3000}
                    hideProgressBar={false}
                    newestOnTop={false}
                    closeOnClick
                    rtl={true}
                    pauseOnFocusLoss
                    draggable
                    pauseOnHover
                />
                <img src={logo} alt="" />
                <form className="login_form" id="login-form" onSubmit={this.handleSubmit}>
                    <label>ایمیل:</label>
                    <input type="password" name="password" placeholder="رمز عبور" onChange={this.handleInputChange} required autoFocus/><br/><br/>
                    <label>رمز عبور:</label>
                    <input type="password" name="password_repeat" placeholder="تکرار رمز عبور" onChange={this.handleInputChange} required autoFocus/><br/><br/>
                    <button type="submit">ورود</button>
                    <br/>
                </form><br/><br/><br/><br/><br/>
            </>
        );
    }

    handleInputChange = (event) => {
        this.setState({ [event.target.name]: event.target.value });
    };

    handleSubmit = async (event) => {
        const url = window.location.pathname.toString().split("/")[2];
        console.log(url);
        event.preventDefault();
        const bearer = "Bearer " + url;
        if(this.state.password === this.state.password_repeat) {
            const apiUrl = `http://87.247.185.122:31304/password`;
            const requestOptions = {
                method: "POST",
                headers: {"Content-Type": "application/json", "Authorization": bearer},
            };

            const response = await fetch(apiUrl, requestOptions);
            const json = await response.json();
            console.log(json.jwtToken);

            if (json.jwtToken !== "") {
                console.log(json.jwtToken);
                document.body.className = "";
                this.props.setId(json.jwtToken);
            }else toast.error("پسورد نامعتبر!");
        } else toast.error("پسورد نامعتبر!");
    };

    getId = () => {
        return (localStorage.getItem("jwt"));
    };
}

export default SetPassword;
