import { React, Component } from "react";
import "./Login.css";
import logo from "../common/photos/logo.png";
import { Redirect } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";


class Password extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            url: "",
        };
        document.body.className = 'login_page';
    }

    render() {
        // const id = this.getId();
        // if (id) return <Redirect to={{ pathname: "/" }} />;
        return (
            <>
                <ToastContainer
                    position="top-center"
                    autoClose={3000}
                    hideProgressBar={false}
                    newestOnTop={false}
                    closeOnClick
                    rtl={true}
                    pauseOnFocusLoss
                    draggable
                    pauseOnHover
                />
                <img src={logo} alt="" />
                <form className="login_form" id="login-form" onSubmit={this.handleSubmit}>
                    <label>ایمیل:</label>
                    <input type="text" name="email" placeholder="آدرس ایمیل" onChange={this.handleInputChange} required autoFocus/><br/><br/>
                    {/*<label>رمز عبور:</label>*/}
                    {/*<input type="password" name="password" placeholder="رمز عبور" onChange={this.handleInputChange} required autoFocus/><br/><br/>*/}
                    <button type="submit">بازیابی رمزعبور</button>
                    <br/>
                    <span className="no-account">
               <a href="/login">خانه</a>
            </span>
                </form><br/><br/><br/><br/><br/>
            </>
        );
    }

    handleInputChange = (event) => {
        this.setState({ [event.target.name]: event.target.value });
    };

    handleSubmit = async (event) => {
        event.preventDefault();
        const apiUrl2 = `http://87.247.185.122:31304/link`;
        const requestOptions2 = {
            method: "POST",
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify({email: this.state.email}),
        };
        const response2 = await fetch(apiUrl2, requestOptions2);
        const json2 = await response2.json();
        if (json2.jwtToken !== "") {
            // console.log(json2.jwtToken);
            document.body.className = "";
            console.log(json2.jwtToken)
            this.setState({url: json2.jwtToken});
            const apiUrl = `http://138.197.181.131:5200/api/send_mail`;
            const requestOptions = {
                method: "POST",
                headers: {"Content-Type": "application/json"},
                body: JSON.stringify(this.state),
            };
            const response = await fetch(apiUrl, requestOptions);
            const json = await response.json();
            // this.props.setId(json2.);
        } else toast.error("ایمیل نامعتبر!");


    };

    getId = () => {
        return (localStorage.getItem("jwt"));
    };
}

export default Password;
